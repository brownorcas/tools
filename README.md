# Orcas Tools

## Install

***

### Download repository

```
git clone https://brownorcas@bitbucket.org/brownorcas/tools.git
```

#### Install dependencies

composer install


## Usage

***

#### Show list of available commands

```
php src/tools
```

### Database configuration replace command

In the first place you need to add database configuration in `src/Config/parameters.yml` then run command
```
php src/tools orcas:database:change DATABASE_NAME
```

### Application build command

In the first place you need to add database configuration in `src/Config/parameters.yml` then run command

Available build types

* behat
* script

```
php src/tools orcas:application:build BUILD_TYPE DATABASE_NAME
```
