<?php

namespace Orcas\Tools\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ContainerAwareCommand
 *
 * @package Orcas\Tools\Command
 */
abstract class ContainerAwareCommand extends Command
{
    use ContainerAwareTrait;
}