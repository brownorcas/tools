<?php

namespace Orcas\Tools\Command;

use Exception;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class BuildApplicationCommand
 *
 * @package Orcas\Tools\Command
 */
class BuildApplicationCommand extends ContainerAwareCommand
{

    /**
     * @var string
     */
    private $melPath;

    /**
     * @var string
     */
    private $melUrl;

    /**
     * @var array
     */
    private $databaseConfig;

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if ($input->getArgument('database')) {
            /** @var QuestionHelper $helper */
            $helper = $this->getHelper('question');

            if (!empty($this->databaseConfig['shared'])) {
                $response = $helper->ask($input, $output,
                    new ConfirmationQuestion("<question>Are you sure you want to clear {$input->getArgument('database')} database? y/n</question>")
                );

                if (!$response) {
                    $output->writeln("<info>Build command stopped</info>");
                    exit(1);
                };
            }
        }
    }


    /**
     * @inheritdoc
     */
    public function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->melUrl = $this->container->getParameter('mel_url');
        $this->melPath = $this->container->getParameter('mel_path');
        $databaseConfig = $this->container->getParameter('database');

        if (empty($databaseConfig[$input->getArgument('database')])) {
            throw new Exception("Given database does not exist in configuration file");
        }

        $this->databaseConfig = $databaseConfig[$input->getArgument('database')];
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('orcas:application:build')
            ->setDescription('Build application')
            ->addArgument('type', InputArgument::REQUIRED, 'behat or script')
            ->addArgument('database', InputArgument::REQUIRED, 'database from configuration');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $buildType = $input->getArgument('type');

        if ($buildType === 'script') {
            $process = $this->getScriptBuildProcess();
        } elseif ($buildType === 'behat') {
            $process = $this->getBashBuildProcess();
        } else {
            throw new Exception("Build type {$buildType} is not supported");
        }

        $process->run(function ($type, $buffer) use ($output) {
            if (Process::ERR === $type) {
                $output->writeln("<error>{$buffer}</error>");
            } else {
                $output->writeln("<info>{$buffer}</info>");
            }
        });

        $output->writeln('<info>Application has been build successfully</info>');
    }

    /**
     * @return Process
     */
    private function getScriptBuildProcess()
    {
        return (new Process("cd {$this->melPath} && ./configure_script.sh"
            . " --prod-db-host='{$this->databaseConfig['database_host']}'"
            . " --prod-db-port='{$this->databaseConfig['database_port']}'"
            . " --prod-db-name='{$this->databaseConfig['database_name']}'"
            . " --prod-db-user='{$this->databaseConfig['database_user']}'"
            . " --prod-db-password='{$this->databaseConfig['database_password']}'"
            . " --content-db-name='{$this->databaseConfig['database_content_name']}'"
            . " --root-url='{$this->melUrl}'"
            . " --optimalisation-off && npm install && grunt build && php app/console assetic:dump")
        )->setTimeout(0);
    }

    /**
     * @return Process
     */
    private function getBashBuildProcess()
    {
        return (new Process("cd {$this->melPath} && ./bin/phing.phar behat-build && time(mysql"
            . " -u{$this->databaseConfig['database_user']} -p'{$this->databaseConfig['database_password']}'"
            . " -e 'DROP database IF EXISTS {$this->databaseConfig['database_name']}'"
            . " && mysql -u{$this->databaseConfig['database_user']} -p'{$this->databaseConfig['database_password']}'"
            . " -e 'DROP database IF EXISTS {$this->databaseConfig['database_content_name']}'"
            . " && mysql -u{$this->databaseConfig['database_content_user']} -p'{$this->databaseConfig['database_content_password']}'"
            . " -e 'CREATE database {$this->databaseConfig['database_name']}'"
            . " && mysql -u{$this->databaseConfig['database_user']} -p'{$this->databaseConfig['database_password']}'"
            . " -e 'CREATE database {$this->databaseConfig['database_content_name']}' "
            . " && mysql -u{$this->databaseConfig['database_content_user']} -p'{$this->databaseConfig['database_content_password']}'"
            . " {$this->databaseConfig['database_name']} < src/IOKI/FunctionalTestCases/Resources/Tests/databases_dumps/userDB.sql"
            . " && mysql -u{$this->databaseConfig['database_content_user']} -p'{$this->databaseConfig['database_content_password']}'"
            . " {$this->databaseConfig['database_content_name']} < src/IOKI/FunctionalTestCases/Resources/Tests/databases_dumps/contentDB.sql)")
        )->setTimeout(0);
    }
}