<?php

namespace Orcas\Tools\Command;

use Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ChangeDatabaseCommand
 *
 * @package Orcas\Tools\Command
 */
class ChangeDatabaseCommand extends ContainerAwareCommand
{

    /**
     * Target configuration path
     */
    const TARGET_CONFIGURATION_PATH = '/app/config/parameters.ini';
    /**
     * @var array
     */
    private $configuration;

    /**
     * @var String
     */
    private $targetConfigurationPath;

    /**
     * @inheritdoc
     */
    public function initialize(InputInterface $input, OutputInterface $output)
    {
        $databaseParameters = $this->container->getParameter('database');

        if (empty($databaseParameters[$input->getArgument('database')])) {
            throw new Exception("Given database does not exist in configuration");
        }

        $this->configuration = $databaseParameters[$input->getArgument('database')];
        $this->targetConfigurationPath = $this->container->getParameter('mel_path') . self::TARGET_CONFIGURATION_PATH;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('orcas:database:change')
            ->setDescription('Change parameters.ini database configuration')
            ->addArgument('database', InputArgument::REQUIRED, 'Database name from configuration');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setNewDatabaseConfiguration();

        $output->writeln('<info>Database configuration has been changed successfully</info>');

    }

    /**
     * Set new configuration to target configuration file
     */
    private function setNewDatabaseConfiguration()
    {
        $targetConfiguration = $this->container->get('utils.reader.ini')->fromFile($this->targetConfigurationPath);

        foreach($this->configuration as $key => $value){
            $targetConfiguration['parameters'][$key] = $value;
        }

        $this->container->get('utils.writer.ini')->toFile($this->targetConfigurationPath, $targetConfiguration);
    }
}